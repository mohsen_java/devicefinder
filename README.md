# USB Device Company Finder

## Overview

This Java program allows you to identify and print the companies associated with USB devices connected to your laptop through the USB port. The program uses the USB API to obtain information about connected devices and matches their vendor numbers with a dataset to determine the company.

## Prerequisites

Before running the program, make sure you have the following:

- Java Development Kit (JDK)
- USB API Library (included in the project)

## Installation

1. Clone the repository from GitLab:

    ```bash
    git clone https://gitlab.com/your-username/usb-device-finder.git
    ```

2. Compile the Java program:

    ```bash
    javac Main.java
    ```

3. Run the program:

    ```bash
    java Main
    ```

## Usage

1. Connect the USB devices to your laptop.
2. Run the Java program.
3. The program will list the connected devices and print the associated company for each device.

## Dataset

The program uses a dataset (vendorList.csv) that contains the names and vendor numbers of various companies. Ensure that this dataset is available in the specified format for accurate company identification.

## Configuration

If needed, you can modify the dataset file path in the `findCompany` method to point to your own dataset:

```java
String filePath = System.getProperty("user.dir") + File.separator + "src" + File.separator + "vendorList.csv";
```

## Contributing

Feel free to contribute to the project by opening issues, providing feedback, or submitting pull requests. Your contributions are highly appreciated!

